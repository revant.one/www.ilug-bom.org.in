---
layout: page
---

<div class="lead lead-about pretty-links">
Upcoming: July 2017 Meetup at D.B.I.T.  
Past Meetup Update: [June 2017 Meetup at Don Bosco](/2017/june-2017-meetup)
</div>
---
### Welcome to ILUG-BOM!
The GNU/Linux Users Group of Mumbai, India, also called ILUG-BOM, is an
informal mailing list of over 1200 members from all over India, but
mostly from Mumbai.

We discuss issues related to promoting, supporting the GNU/Linux OS,
various distributions, sharing free/open source resources, have
informal and formal meetings, seminars and organise workshops now and then. [More &rarr;](/events)
