---
title: Power of Linux - Why you no host?
desc: Simple checklist for linux users to try out self-hosting web services and email server.
author: Revant Nandgaonkar
category: post
---

### Checklist

Start hosting your own website, contacts, calendars, emails and many more services.

#### Purchase a Domain name

Get a domain name from registrar. Choose a provider with easy
and accssible DNS configuration panel. Later these settings need 
to be changed as per the configured VPS.

#### Setup Debian VPS

Get a VPS from reliable provider, this does half the job of infrastructure setup. 
There are free trials as well as other offers for entry level VPS.
VPS must run Debian 8.8 x64 bit. Yunohost only works on Debian check [other projects](https://yunohost.org/#/selfhosting_en) for non Debian distros.

#### Setup reverse DNS to the VPS IP

For emails to work the Reverse DNS lookup should resolve to server IP.
[Amazon](https://aws.amazon.com/blogs/aws/reverse-dns-for-ec2s-elastic-ip-addresses/) and 
[Digitalocean](https://www.digitalocean.com/help/technical/setup/) support Reverse DNS
configurations. [Google](https://issuetracker.google.com/issues/35904529) does not support it.
(click provider link to know more about Reverse DNS status)
#### Install yunohost

Install [yunohost](https://yunohost.org/#/install_on_vps). 
Briefly, login to your VPS and enter following commands

```
sudo apt-get install git dialog
git clone https://github.com/YunoHost/install_script /tmp/install_script
cd /tmp/install_script && sudo ./install_yunohost
```
 
Post install wizard will ask for default domain name (e.g. domain.tld) and set admin password. 
Additional domains can be added later via command line or web UI.

#### Setup DNS settings for domains

For personal use easiest method is,

1. Login to DNS registrar's DNS Setting panel
1. Add `AName` with host `@` pointing to the VPS IP
1. Add `CName` with host `www` pointing to the `@`
1. Back on vps run a command for setting up [Lets Encrypt](https://yunohost.org/#/certmanager) certificate 

```
sudo yunohost domain cert-install domain.tld --force --self-signed
sudo yunohost domain cert-install domain.tld
```
*note the domain.tld is set in post install wizard

Now you can login to your site without any errors or warning messages.
go to https://domain.tld from your browser and enter admin password set in post install wizard

Once your are into the admin dashboard go to Domains > domain.tld > DNS. Here are the detail DNS values to be setup with domain name registrar. Set these values for `TXT`, `AName`, `CNAME`, `SRV` and `MX`.

Example DNS for server ip 1.2.3.4:

```
@ 3600 IN A 1.2.3.4
* 3600 IN A 1.2.3.4

_xmpp-client._tcp 3600 IN SRV 0 5 5222 domain.tld.
_xmpp-server._tcp 3600 IN SRV 0 5 5269 domain.tld.
muc 3600 IN CNAME @
pubsub 3600 IN CNAME @
vjud 3600 IN CNAME @

@ 3600 IN MX 10 domain.tld.
@ 3600 IN TXT "v=spf1 a mx ip4:1.2.3.4 -all"
mail._domainkey.domain.tld. 3600 IN TXT "v=DKIM1; k=rsa; p=HASH/ABC123+PQR321"
_dmarc 3600 IN TXT "v=DMARC1; p=none"

```

#### Add Users

Now Add users for mail boxes and general usage of your hosted services.

#### Add Apps

Add Wordpress for website (www.domain.tld), 
Rainloop to check emails (mail.domain.tld), 
Baikal for Cal/CardDAV server, 
AgenDAV to access calender, 

#### Start using!

Manage web content on wordpress, mobile app also available.
Sync your contacts and events on mobile.
Send and receive mails from web as well as mail clients.
Use Empathy in combination with Vinagre to Share screen (Remote Desktop).
Use jabber clients for voice, video and chat.
