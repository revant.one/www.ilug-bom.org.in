---
title: InstallFest at St. Mary's School
desc: Report from InstallFest conducted at St. Mary's School, Mazgaon.
author: Milind
category: event
---

As all of you know, we had a small InstallFest in St. Mary's School
Mazgaon, Mumbai. We successfully installed Linux (Fedora 25) in over
30 computers of the school's lab.

Our group members came to the school around 1.30 PM. We were pleased
to see our old memeber Rony already there. We really need more members
from ML to participate in this movement.

Sajida Madam from St. Mary's School, Fr. Jude, engineers and
technicians from Ajuman-I-Islam were also present. The enthusiasm &
patience of the school staff and engineers was commendable.

The members namely Nilesh and Tayyab Sir, who reached the venue early,
started copying the customised ISO image prepared by Rajeev to the USB
thumb drives.

We prepared 7-8 bootable Live USB and distributed it to the members
and school staff.

We started the install fest by doing a small demo installation on one
of the computer in the lab. The technicians and teachers took notes as
the install process progressed.

Later everyone started the install process simultaneously on the
computers, to our surprise the staff was successfull in installing
linux with little help. Sajida Madam installed linux on two to three
of the machines all by herself.

We were mostly over seeing and helping them. Meantime we went to one
of the class rooms to examine an education software named "eSense" by
Navneet. It was browser based software so there won't be much problem
in running it on Linux. Mr Walter, the software provider to school,
has already talked to Navneet. Once Navneet comes up with Linux port,
we can install linux in each classroom where one Windows 7 desktop
runs this application at present. Otherwise we might need to port it.

Rony pointed out that such smart school or digital school is only a
marketing gimmick and it has nothing to do it with good
education. Protecting schools from such commercialization is another
social movement. At present we have to fucus on Free software
movement. We can of course discourage schools on using such shiny
tools in place of traditional methods when we interact with
them. Benoy has come up with an idea of making such open source
software as an alternative and distribute in schools.  That is a
wonderful project. We observed that eSense was only collection of
videos based on text book lessons.

We had another surprise visitor in the afternoon, Krishnakant of
Digital Freedom Foundation, he came with Prajakta (a GNUKhata team
memeber). Both were excited to see the enthusiasm of members. Rony was
very happy to see Krishnakant after a long time. Krisnakant told about
latest release of GNUKhata and that the software is now receiving nice
response from government and private sector. "From next academic year,
GNUKahata will be a part of curriculum in Kerala" he said.

Mr Walter (School Software Provider) also showed interest in
GNUKhata. He assured to offer it as an alternative to Tally. Fr Jude
remebered that he had met Krishnkant long ago in IIT mumbai.

Father Jude informed us that board provides a kind of exe file during
exam. So we must write to board to send an alternative for it and
ensure from them that there won't be any issues due to adoption of
Free Software. He is going to write to state board about it. This is
an important point and we must all take it seriously otherwise the
schools will hesitate to adopt FOSS.

Latif, from Osmanabad, was also present for the festival. We have
great hopes from him. Our success certainly depend on how the movement
is adopted in other parts of Maharashtra. We need many more people
like Latif. Thanks to Latif and also thanks to Tayyab for bringing
him.

We completed everything before tea break. Post tea session was a
relaxing time for all of us. We shared our experiences and chat. The
festival ended with a group photo.

*(Report is prepared from the inputs given by Raghavendra Kamath
and other members)*
